import java.lang.Math;
import java.awt.Window;
import java.awt.Frame;
import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

class Vektori
{
  double x;
  double y;

  Vektori( )
  {
	x = 0.0d;
	y = 0.0d;
  }

  public Vektori( double x, double y )
  {
	this.x = x;
	this.y = y;
  }

  public static final Vektori NOLLA = new Vektori( );

  double sisatulo( Vektori kerroin )
  {
	return kerroin.x*this.x + kerroin.y*this.y;
  }

  double ristitulo( Vektori kerroin )
  {
	return this.x*kerroin.y - this.y*kerroin.x;
  }

  double pituus2( )
  {
	return x*x + y*y;
  }

  double pituus( )
  {
	return Math.sqrt( pituus2( ) );
  }

  Vektori kerro( double kerroin )
  {
	return new Vektori( x*kerroin, y*kerroin );
  }

  Vektori lisaa( Vektori lisays )
  {
	return new Vektori( x + lisays.x, y + lisays.y );
  }

  Vektori vahenna( Vektori vahennys )
  {
	return new Vektori( x - vahennys.x, y - vahennys.y );
  }

  Vektori projektio( Vektori v )
  {
	return kerro( sisatulo( v ) / pituus2( ) );
  }

  Vektori peilaa( Vektori v )
  {
	return v.lisaa( projektio( v ).vahenna( v ).kerro( 2.0d ) );
  }

  Yksikkovektori peilaa( Yksikkovektori v )
  {
	return new Yksikkovektori( peilaa( (Vektori) v ) );
  }

  Vektori normaaliPeilaa( Vektori v )
  {
	return v.vahenna( kerro( 2.0d * sisatulo( v ) / pituus2( ) ) );
  }

  Yksikkovektori normaaliPeilaa( Yksikkovektori v )
  {
	return new Yksikkovektori( normaaliPeilaa( (Vektori) v ) );
  }

  double momentti( Suuntajana s )
  {
	return s.haeAlkupiste( ).vahenna( this ).ristitulo( s.haeSuunta( ) );
  }

  @Override
  public String toString( )
  {
	return "<" + x + ", " + y + ">";
  }
}

class Yksikkovektori extends Vektori
{
  Yksikkovektori( double x, double y )
  {
	double r = Math.sqrt( x*x + y*y );
	this.x = x / r;
	this.y = y / r;
  }

  Yksikkovektori( Vektori v )
  {
	double r = v.pituus( );
	this.x = v.x / r;
	this.y = v.y / r;
  }

  Yksikkovektori( double kulma )
  {
	this.x = Math.cos( kulma );
	this.y = Math.sin( kulma );
  }
}

class Suuntajana
{
  protected Vektori alkupiste;
  protected Vektori suunta;

  protected Suuntajana( )
  {
	alkupiste = Vektori.NOLLA;
	suunta = Vektori.NOLLA;
  }

  public Suuntajana( Vektori ap, Vektori s )
  {
	alkupiste = ap;
	suunta = s;
  }

  public Vektori haeAlkupiste( )
  {
	return alkupiste;
  }

  public Vektori haeSuunta( )
  {
	return suunta;
  }

  public Vektori haeLoppupiste( )
  {
	return alkupiste.lisaa( suunta );
  }

  public Vektori projektio( Vektori v )
  {
	return alkupiste.lisaa( suunta.projektio( v.vahenna( alkupiste ) ) );
  }

  @Override
  public String toString( )
  {
	return "< " + haeAlkupiste( ) + " , " + haeSuunta( ) + " >";
  }

  public boolean jatkoLeikkaa( Suuntajana s )
  {
	Vektori v1 = s.haeAlkupiste( ).vahenna( alkupiste );
	Vektori v2 = v1.lisaa( s.haeSuunta( ) );
	return suunta.ristitulo( v1 ) * suunta.ristitulo( v2 ) < 0.0d;
  }

  public boolean leikkaa( Suuntajana s )
  {
	return jatkoLeikkaa( s ) && s.jatkoLeikkaa( this );
  }

  public double leikkauskerroin( Suuntajana s )
  {
  /* alkupiste + kerroin*suunta = leikkauspiste,
   * siis s.suunta.ristitulo( alkupiste - s.alkupiste + kerroin*suunta = 0) */

	Vektori ss = s.haeSuunta( );
	return ss.ristitulo( s.haeAlkupiste( ).vahenna( alkupiste ) )
		/ ss.ristitulo( suunta );
  }

  public Vektori leikkauspiste( Suuntajana s )
  {
	return alkupiste.lisaa( suunta.kerro( leikkauskerroin( s ) ) );
  }

  public static void main( String[ ] argv )
  {
	Random sat = new Random( );
	int j;
	Suuntajana s1, s2;
	Vektori v1, v2;

	System.out.println( "Testi alkaa... " );

	for (j = 1; j < 10; j++) {
		v1 = new Vektori( sat.nextDouble( ), sat.nextDouble( ) );
		v2 = new Vektori( sat.nextDouble( ), sat.nextDouble( ) );
		s1 = new Suuntajana( v1, v2 );
		v1 = new Vektori( sat.nextDouble( ), sat.nextDouble( ) );
		v2 = new Vektori( sat.nextDouble( ), sat.nextDouble( ) );
		s2 = new Suuntajana( v1, v2 );

		double lk21 = s2.leikkauskerroin( s1 );

		if (s1.jatkoLeikkaa( s2 )) {
			if ((lk21 < 0.0d) || (lk21 > 1.0d)) {
				System.out.print( "s1.jL(s2), kerroin = "
						+ lk21 );
			}
		}
		else {
			if ((lk21 > 0.0d) && (lk21 < 1.0d)) {
				System.out.print( "!s1.jL(s2), kerroin = "
						+ lk21 );
			}
		}

		System.out.println( s1.leikkauspiste( s2 )
					.vahenna( s2.leikkauspiste( s1 ) ) );
	}

	System.out.println( "valmis." );
  }
}

class Yksikkosuuntajana extends Suuntajana
{
  protected static final Vektori yv = new Vektori( 1.0d, 0.0d );

  public Yksikkosuuntajana( )
  {
	super( Vektori.NOLLA, yv );
  }

  public Yksikkosuuntajana( Vektori ap, Vektori s )
  {
	super( ap, new Yksikkovektori( s ) );
  }

  public Yksikkosuuntajana( Vektori ap, Yksikkovektori s )
  {
	super( ap, s );
  }

  @Override
  public Yksikkovektori haeSuunta( )
  {
	return (Yksikkovektori) suunta;
  }
}

class YYSuuntajana extends Yksikkosuuntajana
{
  public YYSuuntajana( )
  {
	super( yv, yv );
  }

  public YYSuuntajana( Vektori ap, Vektori s )
  {
	super( new Yksikkovektori( ap ), new Yksikkovektori( s ) );
  }

  public YYSuuntajana( Yksikkovektori ap, Yksikkovektori s )
  {
	super( ap, s );
  }

  @Override
  public Yksikkovektori haeAlkupiste( )
  {
	return (Yksikkovektori) alkupiste;
  }
}

class Ellipsi
{
  private final Vektori koko, pp1, pp2; // koko ilmaisee puoliakselit

  private Vektori laskePolttopiste( )
  {
	double x2 = koko.x * koko.x;
	double y2 = koko.y * koko.y;

	if (x2 <= y2) {
		return new Vektori( 0.0d, Math.sqrt( y2 - x2 ) );
	}
	else {
		return new Vektori( Math.sqrt( x2 - y2 ), 0.0d );
	}
  }

  Ellipsi( )
  {
	koko = Vektori.NOLLA;
	pp1 = Vektori.NOLLA;
	pp2 = Vektori.NOLLA;
  }

  Ellipsi( Vektori v )
  {
	koko = v;
	pp1 = laskePolttopiste( );
	pp2 = pp1.kerro( -1.0d );
  }

  Ellipsi( double x, double y )
  {
	koko = new Vektori( x, y );
	pp1 = laskePolttopiste( );
	pp2 = pp1.kerro( -1.0d );
  }

  public Vektori haeKoko( )
  {
	return koko;
  }

  public Vektori haePolttopiste( )
  {
	return pp1;
  }

  public Vektori skaalaa( Vektori piste )
  {
	return new Vektori( piste.x * koko.x, piste.y * koko.y );
  }

  public Vektori vastaskaalaa( Vektori piste )
  {
	return new Vektori( piste.x / koko.x, piste.y / koko.y );
  }

  public Yksikkovektori seuraavaPiste( Yksikkovektori lahtopiste,
					Yksikkovektori suunta )
  {
	return vastaskaalaa( suunta ).normaaliPeilaa( lahtopiste );
  }

  private Yksikkovektori vsPeilaa( Yksikkovektori u, Yksikkovektori v )
  {
	return vastaskaalaa( v ).normaaliPeilaa( u );
  }

  public YYSuuntajana heijasta( YYSuuntajana tulo )
  {
	Yksikkovektori lahto = tulo.haeAlkupiste( );
	Yksikkovektori tulosuunta = tulo.haeSuunta( );
	Yksikkovektori osuma = vsPeilaa( lahto, tulosuunta );
	Yksikkovektori lahtosuunta = vsPeilaa( tulosuunta, osuma );
	return new YYSuuntajana( osuma, lahtosuunta );
  }

  public YYSuuntajana heijasta( Suuntajana tulo )
	throws IllegalArgumentException
  {
	if (tulo.haeAlkupiste( ).pituus2( ) > 1.0d) {
		throw new IllegalArgumentException( );
	}

	Vektori p = tulo.projektio( Vektori.NOLLA );
	Vektori s = tulo.haeSuunta( );
	double kerroin2 = (1.0d - p.pituus2( )) / s.pituus2( );
	Vektori q = s.kerro( Math.sqrt( kerroin2 ) );
	return heijasta( new YYSuuntajana( p.vahenna( q ), s ) );
  }

  double momentti( Suuntajana s )
  {
	Vektori alku = skaalaa( s.haeAlkupiste( ) );
	Vektori suunta = new Yksikkovektori( s.haeSuunta( ) );
	Suuntajana ss = new Suuntajana( alku, suunta );
	double poikki = Vektori.NOLLA.momentti( ss );
	double pitkin = pp1.sisatulo( suunta );
	double mom = Math.sqrt( poikki*poikki + pitkin*pitkin );
	return mom;
  }
}

class Monikulmio
{
  private final double eps = 1.0e-10d;
  private Vektori[ ] karjet;
  private Suuntajana[ ] sivut;

  protected Monikulmio( )
  {
	karjet = new Vektori[ 0 ];
	sivut = new Suuntajana[ 0 ];
  }

  public Monikulmio( Vektori[ ] k )
  {
	karjet = new Vektori[ k.length + 1 ];
	sivut = new Suuntajana[ k.length ];

	for (int j = 0; j < k.length; j++) {
		karjet[ j ] = k[ j ];
	}

	karjet[ k.length ] = k[ 0 ];

	for (int j = 0; j < k.length; j++) {
		sivut[ j ] = new Suuntajana( karjet[ j ],
			karjet[ j + 1 ].vahenna( karjet[ j ] )
				.kerro( 1.0d + eps ) );
	}
  }

  public Suuntajana heijasta( Suuntajana tulo )
  {
	int j;

	for (j = 0; j < sivut.length; j++) {
		Suuntajana s = sivut[ j ];

		if (tulo.jatkoLeikkaa( s ) && (tulo.leikkauskerroin( s ) > eps)) {
			return new Suuntajana( tulo.leikkauspiste( s ),
						s.haeSuunta( )
							.peilaa( tulo.haeSuunta( ) ) );
		}
	}

	return new Suuntajana( );
  }
}

class Soikiopeili
{
  public static void main( String[ ] argv )
  {
	Random sat = new Random( );
	int marg = 50;
	int puoliLev = 800;
	int puoliKork = 50 + sat.nextInt( 400 );
	Vektori origo = new Vektori( puoliLev + marg, puoliKork + marg );
	Ellipsi soikio = new Ellipsi( puoliLev, puoliKork );
	Window ikkuna = new Window( null, null );
	ikkuna.setSize( 2 * (puoliLev + marg), 2 * (puoliKork + marg) );
	ikkuna.setBackground( Color.black );
	ikkuna.setVisible( true );
	Graphics grafiikka = ikkuna.getGraphics( );
	grafiikka.setColor( Color.yellow );
	grafiikka.drawOval( marg, marg, 2 * puoliLev, 2 * puoliKork );
	Vektori pp1 = soikio.haePolttopiste( );
	Vektori pp2 = pp1.kerro( -1.0d );
	pp1 = pp1.lisaa( origo );
	pp2 = pp2.lisaa( origo );
	grafiikka.setColor( Color.red );
	grafiikka.fillOval( (int) (pp1.x) - 2, (int) (pp1.y) - 2, 4, 4 );
	grafiikka.fillOval( (int) (pp2.x) - 2, (int) (pp2.y) - 2, 4, 4 );
	grafiikka.setColor( Color.white );

	double paikkakulma = sat.nextDouble( ) * Math.PI;
	double suuntakulma = sat.nextDouble( ) * Math.PI;
	Yksikkovektori p = new Yksikkovektori( paikkakulma );
	Yksikkovektori s = new Yksikkovektori( suuntakulma );
	YYSuuntajana kulku = new YYSuuntajana( p, s );
	YYSuuntajana uusiKulku;
	Vektori v1, v2;

	for (int j = 0; j < 1000; j++) {
		uusiKulku = soikio.heijasta( kulku );
		v1 = soikio.skaalaa( kulku.haeAlkupiste( ) ).lisaa( origo );
		v2 = soikio.skaalaa( uusiKulku.haeAlkupiste( ) ).lisaa( origo );
		grafiikka.drawLine( (int) (v1.x), (int) (v1.y),
				(int) (v2.x), (int) (v2.y) );
		kulku = uusiKulku;

		try {
			Thread.sleep( 10 );
		}
		catch (Exception e) {
			ikkuna.dispose( );
			System.err.println( "ups: " + e );
			return;
		}
	}

	try {
		System.in.read( );
	}
	catch (Exception e) {
		ikkuna.dispose( );
		System.err.println( "ups: " + e );
	}

	ikkuna.dispose( );
  }
}

class Kolmiopeili
{
  public static void main( String[ ] argv )
  {
	Random sat = new Random( );
	int marg = 50;
	int puoliLev = 800;
	int puoliKork = 400;
	Vektori origo = new Vektori( puoliLev + marg, puoliKork + marg );
	Vektori[ ] karjet = new Vektori[ 3 ];
	karjet[ 0 ] = new Vektori( puoliLev,puoliKork );
	karjet[ 1 ] = new Vektori( puoliLev * sat.nextDouble( )
					- puoliLev / 2.0d, puoliKork );
	karjet[ 2 ] = new Vektori( -puoliLev, puoliKork * sat.nextDouble( ) );
	karjet[ 1 ] = new Vektori( -puoliLev, puoliKork );
	karjet[ 2 ] = new Vektori( sat.nextDouble( ), -puoliKork );
	Monikulmio kolmio = new Monikulmio( karjet );
	Window ikkuna = new Window( null, null );
	ikkuna.setSize( 2 * (puoliLev + marg), 2 * (puoliKork + marg) );
	ikkuna.setBackground( Color.black );
	ikkuna.setVisible( true );
	Graphics grafiikka = ikkuna.getGraphics( );
	grafiikka.setColor( Color.yellow );
	Vektori u1, u2, u3;
	u1 = origo.lisaa( karjet[ 0 ] );
	u2 = origo.lisaa( karjet[ 1 ] );
	u3 = origo.lisaa( karjet[ 2 ] );
	grafiikka.drawLine( (int) (u1.x), (int) (u1.y),
				(int) (u2.x), (int) (u2.y) );
	grafiikka.drawLine( (int) (u1.x), (int) (u1.y),
				(int) (u3.x), (int) (u3.y) );
	grafiikka.drawLine( (int) (u2.x), (int) (u2.y),
				(int) (u3.x), (int) (u3.y) );
	grafiikka.setColor( Color.white );

	try {
		System.in.read( );
	}
	catch (Exception e) {
		System.err.println( "ups: " + e );
	}

	double paikkakulma = sat.nextDouble( ) * Math.PI;
	double suuntakulma = sat.nextDouble( ) * Math.PI;
	Yksikkovektori p = new Yksikkovektori( paikkakulma );
	Yksikkovektori s = new Yksikkovektori( suuntakulma );
	Suuntajana kulku = new Suuntajana( p, s );
	Suuntajana uusiKulku;
	Vektori v1, v2;

	for (int j = 0; j < 1000; j++) {
		uusiKulku = kolmio.heijasta( kulku );
		v1 = kulku.haeAlkupiste( ).lisaa( origo );
		v2 = uusiKulku.haeAlkupiste( ).lisaa( origo );
		grafiikka.drawLine( (int) (v1.x), (int) (v1.y),
				(int) (v2.x), (int) (v2.y) );
		kulku = uusiKulku;

		try {
			Thread.sleep( 10 );
		}
		catch (Exception e) {
			ikkuna.dispose( );
			System.err.println( "ups: " + e );
			return;
		}
	}

	try {
		System.in.read( );
	}
	catch (Exception e) {
		ikkuna.dispose( );
		System.err.println( "ups: " + e );
	}

	ikkuna.dispose( );
  }
}
